var express = require('express');
var app = express();

app.use(express.static('public'));

app.get('/', (request, response) => {
	response.render('index.html');
})

//app.get('js', (request, response) => {
//	response.sendfile('./public/')
//})

app.listen(8080);
